//Merge two sorted arrays
//ES6 version
const mergeSortedArrays = (arr1, arr2) => arr1.length && arr2.length ? arr1.concat(arr2).sort((a, b) => a - b) : undefined


const result = mergeSortedArrays([0, 3, 4, 31], [4, 6, 30]);
console.log("result", result)

//For loop version
const mergeSortedArrays2 = (arr1, arr2) => {
let newArray = [];
if (arr1.length && arr2.length) {
    for (let i = 0; i < arr1.length; i++) {
        const element = arr1[i];
        newArray.push(element)
    }
   for (let i = 0; i < arr2.length; i++) {
        const element = arr2[i];
        newArray.push(element)
    }
    for (let i = 0; i < newArray.length; i++) {
        const a = newArray[i];
        for (let j = 0; j < newArray.length; j++) {
            const b = newArray[j];
            if(a < b) {
                if(i > j) {
                    newArray.push(b);
                    newArray.splice(j, 1);
                    break;
                }
            }
        }   
    }
    return newArray;
    } else return undefined
}

const result2 = mergeSortedArrays2([0, 3, 4, 31], [4, 6, 30]);
console.log("result2", result2)