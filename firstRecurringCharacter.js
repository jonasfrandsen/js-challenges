//Given an array = [2,5,1,2,3,5,1,2,4]:
//It should return 2

//Given an array = [2,1,1,2,3,5,1,2,4]:
//It should return 1

//Given an array = [2,3,4,5]:
//It should return undefined

//Bonus... What if we had this:
// [2,5,5,2,3,5,1,2,4]
// return 5 because the pairs are before 2,2


function firstRecurringCharacter(input) {
    let count = new Map();

    //input validation
    if(input.length) {
        //loop through input array
        for (let i = 0; i < input.length; i++) {
            const element = input[i];
            //if a key that is equal to "element" already exists on Map, then...
            if(count.has(element)) {
                //...set the value of key "element" to the current value plus 1
                count.set(element, count.get(element) +1);
            }
            //if a key that is equal to "element" already exists on Map and the value that key is strictly equal to 2, then... 
            if(count.has(element) && count.get(element) === 2) {
                //...assign element value to result and break out of loop
                return element
            }
            //if a key that is equal to "element" does not exist on Map, then add it
            count.set(element, 1)
        }
        //if variable result is not empty return result else return undefined
    }
    return undefined;
};

const result1 = firstRecurringCharacter([2,5,1,2,3,5,1,2,4]);
const result2 = firstRecurringCharacter([2,1,1,2,3,5,1,2,4]);
const result3 = firstRecurringCharacter([2,3,4,5]);
const result4 = firstRecurringCharacter([2,5,5,2,3,5,1,2,4]);
console.log("result1", result1);
console.log("result2", result2);
console.log("result3", result3);
console.log("result4", result4);

