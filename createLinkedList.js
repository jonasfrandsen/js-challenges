// Create the below linked list:
// myLinkedList = {
//   head: {
//     value: 10
//     next: {
//       value: 5
//       next: {
//         value: 16
//         next: null
//       }
//     }
//   }
// };

class Node {
    constructor(value){
        this.value = value,
        this.next = null
    }
}

class LinkedList {
    constructor(value){
        this.head = {
            value: value,
            next: null,
        },
        this.tail = this.head,
        this.length = 1
    }
    append(value) {
        const newTail = new Node(value)
        this.tail.next = newTail;
        this.tail = newTail;
        this.length++;
    }
    prepend(value) {
        const newHead = new Node(value)
        let oldHead = this.head;
        this.head = newHead;
        this.head.next = oldHead;
        this.length++;
    }
    printList() {
        const array = [];
        let currentNode = this.head;
        while(currentNode !== null){
            array.push(currentNode.value)
            currentNode = currentNode.next
        };
        return array;
      }
      insert(index, value) {
        if(index === 0) {
            this.prepend(value);
            return this.printList();
        } else if(index >= this.length) {
            this.append(value);
            return this.printList();
        } else {
            let pre = this.head;
            for (let j = 0; j < index -1; j++) {
                pre = pre.next;
            };
            let aft = pre.next;
            let newNode = new Node(value);
            pre.next = newNode;
            newNode.next = aft;
            this.length++;
            return this.printList();
        }
      }
      remove(index) {
        let pre = this.head;
        let del;
        let aft;
        let temp;
        if(index === 0) {
            this.head = pre.next;
            this.length--;
            return this.printList();
        }
        if (index >= this.length) {
            temp = this.head.next;
            while (temp.next !== null) {
                pre = pre.next;
                console.log("pre", pre)
            }
            console.log("pre done", pre)
            pre.next = null;
            this.tail = pre;
            this.length--;
            return this.printList();
        }
        if (index > 0 && index < this.length) {
            for (let j = 0; j <= index + 1; j++) {
                if(j < index - 1) {
                    pre = pre.next
                }
                if(j === index) {
                    del = pre.next
                }
            };
            aft = del.next;
            pre.next = aft;
            this.length--
            return this.printList();
        }
       
    }
}

const myLinkedList = new LinkedList(1);
myLinkedList.append(2);
myLinkedList.append(3);
myLinkedList.append(4);
myLinkedList.append(5);
myLinkedList.append(6);
console.log(myLinkedList)
myLinkedList.remove(200)

console.log(myLinkedList)