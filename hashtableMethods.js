class HashTable {
    constructor(size){
      this.data = new Array(size);
    }
  
    _hash(key) {
      let hash = 0;
      for (let i = 0; i < key.length; i++){
          hash = (hash + key.charCodeAt(i) * i) % this.data.length
      }
      return hash;
    }
    set(key, value){
        const keyValuePairArray = [key, value];
        const index = this._hash(key);
        if(!this.data[index]) {
            this.data[index] = [];
            this.data[index].push(keyValuePairArray);
            
        } else this.data[index].push(keyValuePairArray);
        
        
    };
    get(arg){
        const index = this._hash(arg);
        const current = this.data[index];
        let result;
        if (current) {
        for (let i = 0; i < current.length; i++) {
            const element = current[i];
            if(element[0] === arg) {
                result = element[1];
            }
        }
    } else result = undefined
        return result;
    }
    keys(){
        const result = [];
        for (let i = 0; i < this.data.length; i++) {
            const element = this.data[i];
            if(element) {
                result.push(element[0][0]);
            }
        }
        return result
    }
  }
  
  const myHashTable = new HashTable(50);
  myHashTable.set('grapes', 10000) 
  const result1 = myHashTable.get('grapes')
  myHashTable.set('apples', 9)
  const result2 = myHashTable.get('apples')
  const keys = myHashTable.keys();

  console.log("myHashTable", myHashTable)

  console.log("result1", result1)
  console.log("result2", result2)
  console.log("keys", keys);