//Reverse a string
function reverse(str) {
    if (str || typeof str === "string") {
        const reverse = (arr) => {
            let newArray = [];
            for (let i = 0; i < arr.length; i++) {
                const element = arr[i];
                newArray.unshift(element);
            }
            return newArray
        }
        const reversedArray = reverse(str)
        const arrayToString = reversedArray.join("");
        return arrayToString
    } else return undefined
}

const result = reverse("Reverse this string");
console.log(result);

//simplified version
function reverse2(str) {
    if (str || typeof str === "string") {
        return str.split("").reverse().join("");
    } else return undefined
}

const result2 = reverse2("Also reverse this string");
console.log(result2);

//ES6 version
const reverse3 = (str) => str && typeof str === "string" ? str.split("").reverse().join("") : undefined;

const result3 = reverse3("Lastly reverse this string");
console.log(result3);